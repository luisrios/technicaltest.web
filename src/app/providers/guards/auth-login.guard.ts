import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SecurityService } from '../services/security/security.service';

@Injectable({
  providedIn: 'root',
})
export class AuthLoginGuard implements CanActivate {
  constructor(
    private router: Router,
    private readonly securityService: SecurityService
  ) {
    // ctor
  }

  canActivate():
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let user = this.securityService.getUserData();
    if (user !== null) {
      this.router.navigate(['']);
      return false;
    } 
    return true;
    // const currentUser = this.localStorageService.getItemObject<{ modules: Array<number> }>('currentUser');
    // if (!currentUser || !currentUser.modules.some((m) => m === MODULES.ADMIN)) {
    //   this.router.navigate(['not-authorization']);
    //   return false;
    // }
  }
}
