import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiBaseService } from 'src/core/shared/common/services/api';

@Injectable()
export class ApiService extends ApiBaseService {
  constructor(httpClient: HttpClient) {
    super(httpClient);
  }
}