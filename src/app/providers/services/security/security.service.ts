import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LocalStorageService } from '../../../../core/shared/common/services/storage/local-storage.service';
import { KEYS } from 'src/core/shared/common/services/storage/keys-data.enum';
import { ApiService } from '../api/api.service';
import { IAuthUserPerson, IAuthResponse, IAuthRequest, IAuthResponseData } from '../../../settings/models/auth.interface';
import { SecurityEndpoint } from '../../endpoints/security.endpoint';
import { ObjectUtil } from '../../../../core/shared/helpers/util/object';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
  })
export class SecurityService {

    isAutentication = false;

    constructor(
        private storageService: LocalStorageService,
        private readonly apiService: ApiService,
        private route: Router
    ){
        this.userDataSubject = new BehaviorSubject<IAuthResponseData>(this.getUserData());
    }

    userDataSubject: BehaviorSubject<IAuthResponseData>;

    setUserData(data: IAuthResponseData) {
        const userData = this.getUserData();
        if (!ObjectUtil.isEquals(userData, data)) {
          this.storageService.setItem(KEYS.USERDATA, data);
          this.userDataSubject.next(data);
        }
    }
    setUserDataFinanzas(data: IAuthResponseData) {
        const userData = this.getUserData();
        if (!ObjectUtil.isEquals(userData, data)) {
          this.storageService.setItem(KEYS.USERDATAFINANZAS, data);
          this.userDataSubject.next(data);
        }
    }
    getUserData(): IAuthResponseData {
        let userStorage = this.storageService.getItem(KEYS.USERDATA);
        return userStorage !== "" ? JSON.parse(this.storageService.getItem(KEYS.USERDATA)) : null;
    }


    getUserDataSubject(): BehaviorSubject<IAuthResponseData> {
        return this.userDataSubject;
    }

    login(obj: IAuthRequest): Observable<IAuthResponse> {
        return this.apiService.post<IAuthResponse>(`${SecurityEndpoint.login}`, obj);
    }


    isAuthentication(): boolean {
        return this.getUserData() != null;
    }

    setAuthentication(auth: boolean): void {
        this.isAutentication = auth;
    }


    logoutKeys(){
        this.isAutentication = false;
        this.setUserData({});
        localStorage.removeItem(KEYS.USERDATA);
    }
}