import { environment } from "src/environments/environment";

export class SecurityEndpoint {
  public static login = `${environment.apiGateway}/seguridad/seguridad/autenticar`;
}