import { ApplicationRef, Component, HostListener, OnInit } from '@angular/core';
import { CODE_RESPONSE_VALUES } from './settings/constants/code-response-values.constants';
import { SwUpdate } from '@angular/service-worker';
import { interval } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Front';

  color = 'bgWhite';
  showPopUp = false;
  // Declare height and width variables
  scrHeight: any;
  scrWidth: any;
  @HostListener('window:resize', ['$event'])
  getScreenSize() {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    // console.log(this.scrHeight, this.scrWidth);
  }
  constructor(private update: SwUpdate, private appRef: ApplicationRef) {
    this.getScreenSize();
    console.log('UpdateService: Constructor', this.update.isEnabled);

    interval(10000).subscribe(() => {
      console.log('UpdateService: Checking for Updates');
      this.update.checkForUpdate();
    });

    this.update.versionUpdates.subscribe(async (evt) => {
      console.log('UpdateService: versionUpdates', evt);
      switch (evt.type) {
        case 'VERSION_DETECTED':
          console.log(`Downloading new app version: ${evt.version.hash}`);
          break;
        case 'VERSION_READY':
          console.log(`Current app version: ${evt.currentVersion.hash}`);
          console.log(
            `New app version ready for use: ${evt.latestVersion.hash}`
          );
          await this.update.activateUpdate();
          location.reload();
          break;
        case 'VERSION_INSTALLATION_FAILED':
          console.log(
            `Failed to install app version '${evt.version.hash}': ${evt.error}`
          );
          break;
      }
    });
  }

  ngOnInit(): void {}

  closePopupLogin() {}
}
