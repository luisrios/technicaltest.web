import { NgModule } from '@angular/core';
import { DatePipe, LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainLayoutModule } from './main/layouts/main-layout/main-layout.module';
import { ApiModule } from './providers/services/api';
import { LoadingInterceptor } from '../core/shared/common/interceptor/loading.interceptor';
import { LoadingModule } from '../core/ui/components/loading/loading.module';
import { ErrorInterceptor } from '../core/shared/common/interceptor/error.interceptor';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ToastModule } from 'primeng/toast';
import { GoogleTagManagerModule } from 'angular-google-tag-manager';
import { LoginModule } from './main/views/security/login/login.module';
import { LoginLayoutComponent } from './main/layouts/login-layout/login-layout.component';
import { LoginLayoutModule } from './main/layouts/login-layout/login-layout.module';
import { AuthGuard } from './providers/guards/auth.guard';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MainLayoutModule,
    LoginLayoutModule,
    ApiModule,
    LoadingModule,
    ToastModule,
    NgxCaptchaModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
