import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './main/layouts/main-layout/main-layout.component';
import { AuthGuard } from './providers/guards/auth.guard';
import { LoginLayoutComponent } from './main/layouts/login-layout/login-layout.component';
import { LoginComponent } from './main/views/security/login/login.component';
import { AuthLoginGuard } from './providers/guards/auth-login.guard';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        canActivate: [AuthGuard],
        loadChildren: () =>
          import('./main/views/main-home/main-home.module').then(
            (m) => m.MainHomeModule
          ),
      },
      {
        path: 'login',
        canActivate: [AuthLoginGuard],
        loadChildren: () =>
          import('./main/views/security/login/login.module').then(
            (m) => m.LoginModule
          ),
      },
      { path: '', pathMatch: 'full', redirectTo: '' },
      { path: '**', redirectTo: 'pages/error-404' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
