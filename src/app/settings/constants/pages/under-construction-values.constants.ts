export const UNDER_CONSTRUCTION_VALUES = {
    title: '<div class="titulo">Estamos <br> trabajando para ti</div><div class="message">No disponible para dispositivos de computadora de escritorio, laptop o tablet en modo horizontal.</div><div class="message">Por favor ingresa desde un dispositivo mobile.</div>',
    buttonOkText: 'Ir al home',
    image: '../../../../../../assets/img/408.png'
}

export const ERROR_500_VALUES = {
    title: '<div class="titulo">Error 500</div><div class="message">Algo salió mal.</div><div class="message">Se ha producido un error y estamos trabajando para solucionar el problema.</div>',
    buttonOkText: 'Ir al home',
    image: './assets/img/error-500.png'
}

export const ERROR_400_VALUES = {
    title: '<div class="titulo">Error 400</div><div class="message">Solicitud incorrecta.</div><div class="message">Lo sentimos, el servidor no pudo procesar la solicitud.</div>',
    buttonOkText: 'Ir al home',
    image: './assets/img/error-400.png'
}

export const ERROR_404_VALUES = {
    title: '<div class="titulo">Error 404</div><div class="message">La página que estás buscando no existe.</div><div class="message">Es posible que exista un error en la dirección o que la página no esté disponible.</div>',
    buttonOkText: 'Ir al home',
    image: './assets/img/error-404.png'
}

export const CONSTRUCTION_FORYOU = {
    title: '<div class="titulo">Estamos mejorando tu<br>experiencia</div><div class="message">Marketplace no se encuentra disponible para computadoras, laptops y tablets.</div><div class="message mt-3">Te recomendamos ingresar desde tu dispositivo móvil.</div>',
    buttonOkText: '',
    image: './assets/svg/estamos-trabajando.svg'
}