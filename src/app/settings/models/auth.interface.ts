import { IResponseInterface } from './response.interface';

export interface IAuthRequest {
  Correo?: string;
  Clave?: string;
}

export interface IAuthResponseData {
  Token?: IAuthToken;
  Usuario?: IAuthUserPerson;
}

export interface IAuthToken {
  Value?: string;
  FechaExpiracion?: string;
}

export interface IAuthUserPerson {
  UsuarioId?: number;
  Mombres?: string;
  Apellidos?: string;
  Correo?: string;
  Codigo?: string;
}


export interface IAuthResponse extends IResponseInterface<IAuthResponseData> { };
