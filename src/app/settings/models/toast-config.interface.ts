export interface IToastConfig {
    position?: string;
    key?: string;
    severity?: string;
    summary? : string;
    detail?: string;
    textClick?:string;
    baseZIndex?:number;
    life?:number;
}