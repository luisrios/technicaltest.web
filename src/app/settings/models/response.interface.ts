export interface IResponseInterface<T> {
    Status?: number;
    Message?: string;
    Data?: T;
    TransactionId?:string;
}