import { Component, OnDestroy, OnInit } from '@angular/core';
import { IntermediarioService } from '@core/shared/common/services/intermediary/intermediario.service';
import { SignalRService } from '@core/shared/common/services/signalr/signalr.service';

@Component({
  selector: 'tec-main-home',
  templateUrl: './main-home.component.html',
  styleUrls: ['./main-home.component.scss'],
})
export class MainHomeComponent implements OnInit, OnDestroy {
  constructor(
    private signalRService: SignalRService,
    public intermediarioService: IntermediarioService
  ) {}

  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }
  ngOnInit(): void {
    this.signalRService.startConnection();
    this.signalRService.addEvent();
  }
}
