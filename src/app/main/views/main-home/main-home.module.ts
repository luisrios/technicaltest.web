import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainHomeComponent } from './main-home.component';
import { MainHomeRoutingModule } from './main-home.routing';
import { FormsModule } from '@angular/forms';
import { ToastViewModule } from 'src/core/ui/components/toast-view/toast-view.module';

@NgModule({
  declarations: [MainHomeComponent],
  imports: [
    CommonModule,
    MainHomeRoutingModule,
    FormsModule,
    ToastViewModule,
  ],
})
export class MainHomeModule { }
