import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserHomeRoutingModule } from './user-home.routing';

@NgModule({
  imports: [
    CommonModule,
    UserHomeRoutingModule
  ],
  exports: [],
})
export class UserHomeModule {}