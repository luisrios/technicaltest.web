import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecurityRoutingModule } from './security.routing.module';

@NgModule({
  imports: [
    CommonModule,
    SecurityRoutingModule
  ],
  exports: [],
})
export class SecurityModule {}