import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  ValidationErrors,
  AbstractControl,
} from '@angular/forms';
import { SecurityService } from '../../../../providers/services/security/security.service';
import {
  IAuthRequest,
  IAuthResponse,
  IAuthResponseData,
} from '../../../../settings/models/auth.interface';
import { CODE_RESPONSE_VALUES } from '../../../../settings/constants/code-response-values.constants';
import { Router } from '@angular/router';
import { LocalStorageService } from '../../../../../core/shared/common/services/storage/local-storage.service';
import { KEYS } from '../../../../../core/shared/common/services/storage/keys-data.enum';
import { HostListener } from '@angular/core';
import { authConfig } from 'src/app/settings/auth/auth.config';
@Component({
  selector: 'eco-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  titlePage = 'Iniciar Sesión';
  loginForm!: UntypedFormGroup;
  submmittedEmail: boolean = false;
  submmittedPassword: boolean = false;
  validEmail = '1';

  // Declare height and width variables
  scrHeight: any;
  scrWidth: any;
  public showPassword: boolean = false;
  @HostListener('window:resize', ['$event'])
  getScreenSize() {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
  }

  keyCaptcha: string = authConfig.keyGoogleCaptcha;
  validCaptcha: boolean = authConfig.validCaptcha;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private securityService: SecurityService,
    private storageService: LocalStorageService,
    private router: Router
  ) {
    this.getScreenSize();
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.createForm();
  }

  createForm(): void {
    this.loginForm = this.formBuilder.group({
      correo: [
        '',
        [
          Validators.required,
          Validators.pattern(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ],
      ],
      clave: ['', [Validators.required]],
    });
    if (!this.validCaptcha) {
      this.loginForm.controls['recaptcha'].patchValue('no-captcha');
    }
  }

  login(): void {
    if (this.loginForm.valid) {
      const data = this.loginForm.value;
      const request: IAuthRequest = {
        Correo: data.correo,
        Clave: data.clave,
      };
      this.securityService.login(request).subscribe(
        (response: IAuthResponse) => {
          console.log("login",response);
          if (response.Status === CODE_RESPONSE_VALUES.ok) {
            console.log(response);
            this.securityService.setUserData(response.Data || {});
            this.router.navigate(['/']);
          } else {
            console.log(response.Message);
          }
        },
        (error: any) => {}
      );
    }
  }

  onSelectInput(id: number): void {
    const data = this.loginForm.value;
    switch (id) {
      case 1:
        this.submmittedEmail = data.email;
        this.validEmail = data.email;
        break;
      case 2:
        this.submmittedPassword = data.password == '';
        break;
    }
  }

  handleSuccess(e: string) {
    this.loginForm.controls['recaptcha'].patchValue(e || '');
  }

  handleExpire() {
    this.loginForm.controls['recaptcha'].patchValue('');
  }
}
