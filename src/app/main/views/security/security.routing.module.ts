import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLoginGuard } from 'src/app/providers/guards/auth-login.guard';

const routes: Routes = [
  {
    path: '',
    canActivate:[AuthLoginGuard],
    loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule {}