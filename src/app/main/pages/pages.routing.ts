import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'under-construction',
    loadChildren: () => 
      import('./under-construction/under-construction.module').then((m) => m.UnderConstructionModule),
  },
  {
    path: 'error-404',
    loadChildren: () => 
      import('./error-404/error-404.module').then((m) => m.Error404Module),
  },
  {
    path: 'error-400',
    loadChildren: () => 
      import('./error-400/error-400.module').then((m) => m.Error400Module),
  },
  {
    path: 'error-500',
    loadChildren: () => 
      import('./error-500/error-500.module').then((m) => m.Error500Module),
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}