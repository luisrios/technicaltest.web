import { Component } from '@angular/core';
import { IPagesInformation } from '../../../../core/ui/components/pages-information/pages-information.interface';
import { ERROR_404_VALUES } from '../../../settings/constants/pages/under-construction-values.constants';

@Component({
    selector: 'eco-error-404',
    templateUrl: './error-404.component.html',
    styleUrls: ['./error-404.component.scss'],
  })
export class Error404Component{

  pageInformation: IPagesInformation = {}

  constructor() {
    this.pageInformation.title = ERROR_404_VALUES.title;
    this.pageInformation.image = ERROR_404_VALUES.image;
    this.pageInformation.buttonOkText = ERROR_404_VALUES.buttonOkText;
    window.scrollTo(0,0);
  }
}