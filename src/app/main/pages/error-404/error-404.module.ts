import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesInformationModule } from '../../../../core/ui/components/pages-information/pages-information.module';
import { Error404Component } from './error-404.component';
import { Error404RoutingModule } from './error-404.routing';

@NgModule({
  declarations: [Error404Component],
  imports: [CommonModule, Error404RoutingModule, PagesInformationModule],
})
export class Error404Module {}