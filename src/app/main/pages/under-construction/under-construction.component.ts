import { Component } from '@angular/core';
import { IPagesInformation } from '../../../../core/ui/components/pages-information/pages-information.interface';
import { UNDER_CONSTRUCTION_VALUES } from '../../../settings/constants/pages/under-construction-values.constants';

@Component({
    selector: 'eco-under-construction',
    templateUrl: './under-construction.component.html',
    styleUrls: ['./under-construction.component.scss'],
  })
export class UnderConstructionComponent{

  pageInformation: IPagesInformation = {}

  constructor() {
    this.pageInformation.title = UNDER_CONSTRUCTION_VALUES.title;
    this.pageInformation.image = UNDER_CONSTRUCTION_VALUES.image;
    this.pageInformation.buttonOkText = UNDER_CONSTRUCTION_VALUES.buttonOkText;
    window.scrollTo(0,0);
  }
}