import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnderConstructionComponent } from './under-construction.component';
import { PagesInformationModule } from '../../../../core/ui/components/pages-information/pages-information.module';
import { UnderConstructionRoutingModule } from './under-construction.routing';

@NgModule({
  declarations: [UnderConstructionComponent],
  imports: [CommonModule, UnderConstructionRoutingModule, PagesInformationModule],
})
export class UnderConstructionModule {}