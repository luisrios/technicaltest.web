import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesInformationModule } from '../../../../core/ui/components/pages-information/pages-information.module';
import { Error400Component } from './error-400.component';
import { Error400RoutingModule } from './error-400.routing';


@NgModule({
  declarations: [Error400Component],
  imports: [CommonModule, Error400RoutingModule, PagesInformationModule],
})
export class Error400Module {}