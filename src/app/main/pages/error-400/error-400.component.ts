import { Component } from '@angular/core';
import { IPagesInformation } from '../../../../core/ui/components/pages-information/pages-information.interface';
import { ERROR_400_VALUES } from '../../../settings/constants/pages/under-construction-values.constants';

@Component({
    selector: 'eco-error-400',
    templateUrl: './error-400.component.html',
    styleUrls: ['./error-400.component.scss'],
  })
export class Error400Component{

  pageInformation: IPagesInformation = {}

  constructor() {
    this.pageInformation.title = ERROR_400_VALUES.title;
    this.pageInformation.image = ERROR_400_VALUES.image;
    this.pageInformation.buttonOkText = ERROR_400_VALUES.buttonOkText;
    window.scrollTo(0,0);
  }
}