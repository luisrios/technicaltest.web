import { Component } from '@angular/core';
import { IPagesInformation } from '../../../../core/ui/components/pages-information/pages-information.interface';
import { UNDER_CONSTRUCTION_VALUES, ERROR_500_VALUES } from '../../../settings/constants/pages/under-construction-values.constants';

@Component({
    selector: 'eco-error-500',
    templateUrl: './error-500.component.html',
    styleUrls: ['./error-500.component.scss'],
  })
export class Error500Component{

  pageInformation: IPagesInformation = {}

  constructor() {
    this.pageInformation.title = ERROR_500_VALUES.title;
    this.pageInformation.image = ERROR_500_VALUES.image;
    this.pageInformation.buttonOkText = ERROR_500_VALUES.buttonOkText;
    window.scrollTo(0,0);
  }
}