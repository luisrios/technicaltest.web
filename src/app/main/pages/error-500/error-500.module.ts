import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesInformationModule } from '../../../../core/ui/components/pages-information/pages-information.module';
import { Error500Component } from './error-500.component';
import { Error500RoutingModule } from './error-500.routing';

@NgModule({
  declarations: [Error500Component],
  imports: [CommonModule, Error500RoutingModule, PagesInformationModule],
})
export class Error500Module {}