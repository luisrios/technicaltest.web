import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, throttleTime } from 'rxjs';
import { IPagesInformation } from '../../../../core/ui/components/pages-information/pages-information.interface';
import { CONSTRUCTION_FORYOU } from '../../../settings/constants/pages/under-construction-values.constants';

@Component({
  selector: 'eco-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
  ngOnInit(): void {}
}
