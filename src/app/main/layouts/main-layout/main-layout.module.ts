import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MainLayoutComponent } from './main-layout.component';
import { HeaderModule } from '../../../../core/ui/components/header/header.module';
import { PagesInformationModule } from 'src/core/ui/components/pages-information/pages-information.module';

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    HeaderModule,
    PagesInformationModule
  ],
})
export class MainLayoutModule {}