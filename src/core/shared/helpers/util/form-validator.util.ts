import { AbstractControl, ValidatorFn } from '@angular/forms';

export function NoSecondCharSpaceValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const isSecondCharSpace = control.value ? control.value.charAt(1) === ' ' : false;
    return isSecondCharSpace ? { 'secondCharSpace': {value: control.value}} : null;
  };
}