export class KeyValidator {

  static onKeypressEventNumber(event: any): boolean {
      const regex = new RegExp("[0-9,]$");
      if(!regex.test(String.fromCharCode(event.keyCode))){
        event.preventDefault();
        return false;
      }
      return true;
    }

    static onKeypressEventText(event: any): boolean {
      const regex = new RegExp("^[a-zA-ZÀ-ž ]*$");
      if(!regex.test(String.fromCharCode(event.keyCode))){
        event.preventDefault();
        return false;
      }
      return true;
    }
  }