export class FormatUtil {
  static Text(id:number) {
    return this.formatNumberWithLeadingZeros(id,5);
  }
  private static formatNumberWithLeadingZeros(numero: number, width: number): string {
    return numero.toString().padStart(width, '0');
  }
  static removeAccents(str: string): string {
    const accentsMap: { [key: string]: string } = {
        'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a',
        // ... rest of the mapping
    };

    return str.split('').map(c => accentsMap[c] || c).join('');
  }
}
