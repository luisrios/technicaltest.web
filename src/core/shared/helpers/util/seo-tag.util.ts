
import { Meta } from '@angular/platform-browser';

export class SeoTagUtil { 

    constructor(private metaService: Meta) { }

    public updateOrCreateMetaTag(tag: any, selector: string): void {
        const existingTag = this.metaService.getTag(selector);
        if (existingTag) {
          this.metaService.updateTag(tag, selector);
        } else {
          this.metaService.addTag(tag);
        }
      }
}
