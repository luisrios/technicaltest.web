import { isEqual } from 'lodash-es';

export class ObjectUtil {
  public static clone(object: any): any {
    return JSON.parse(JSON.stringify(object));
  }

  public static isEquals(object1: any, object2: any): boolean {
    return isEqual(object1, object2);
  }
}