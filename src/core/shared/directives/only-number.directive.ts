import { Directive, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[onlyNumber]'
})
export class OnlyNumberDirective {

  @Input() onlyNumber!: boolean;

  @HostListener('keydown', ['$event']) onKeyDown(event: any) {
      try {
          this.validateOnlyNumber(event)
      } catch (error) {
      }

  }

  private validateOnlyNumber(event: any): any {
      let charCode = (event.which) ? event.which : event.keyCode;
      var ctrlDown = event.ctrlKey || event.metaKey
      // CONTROL + C
      if (ctrlDown && charCode == 67) {
          return false;
      }
      let value = event.target.value;
      if (ctrlDown && charCode == 86) {
          setTimeout(() => {
              if (isNaN(event.target.value)) {
                  event.target.value = value;
                  event.preventDefault();
              }
          }, 1);
          return false;
      } else {
          if (charCode != 46 && charCode != 190 && charCode != 110 && charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && (charCode < 37 || charCode > 40)) {
              event.preventDefault();
              return false;
          }
      }
      return true;
  }

}
