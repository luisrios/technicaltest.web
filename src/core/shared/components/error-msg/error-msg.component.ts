import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidatorsExtend } from '@core/shared/common/validators/validators-extend';

@Component({
  selector: 'eco-error-msg',
  templateUrl: './error-msg.component.html',
  styleUrls: ['./error-msg.component.css']
})
export class ErrorMsgComponent {

  @Input() control!: FormControl;
  @Input() notContainer = false;
  
  constructor() {}

  get errorMessage() {
    for (const propertyName in this.control.errors) {
        if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
            return ValidatorsExtend.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
        }
    }
    return null;
  }

}
