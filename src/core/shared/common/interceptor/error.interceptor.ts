import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, tap } from 'rxjs';
import { SecurityService } from "src/app/providers/services/security/security.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor( private router: Router, private securityService: SecurityService) {}
  
    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        return next.handle(request).pipe( tap(() => {},
    (err: any) => {
    if (err instanceof HttpErrorResponse) {
      if(err.status === 400) {
        this.router.navigate(['/pages/error-400']);
      } else if(err.status === 500) {
        this.router.navigate(['/pages/error-500']);
      } else if(err.status === 404) {
        this.router.navigate(['/pages/error-404']);
      } else if(err.status === 401) {
        //this.securityService.logout();
      }
    }
  }));
    }
}