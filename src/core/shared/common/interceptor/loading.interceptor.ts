import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, finalize } from 'rxjs/operators';
import { LoadingService } from '../services/intermediary/loading.service';
import { SecurityService } from '../../../../app/providers/services/security/security.service';
import { authConfig } from 'src/app/settings/auth/auth.config';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  constructor(
    private loadingService2: LoadingService,
    private securityService: SecurityService
  ) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    let token = this.securityService.getUserData()?.Token?.Value || '';

    let keyBasic = `${authConfig.clientId}:${authConfig.clientSecret}`;

    if (token == '') {
      request = request.clone({
        setHeaders: {
          AuthorizationBasic: `Basic ${btoa(keyBasic)}`,
        },
      });
    }

    if (token !== '') {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
          AuthorizationBasic: `Basic ${btoa(keyBasic)}`,
        },
      });
    }
    if(!request.url.includes("autocomplete-products-store")){
      this.loadingService2.busy();
    }
    return next.handle(request).pipe(
      finalize(() => {
        this.loadingService2.idle();
      })
    );
  }
}
