import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import lottie from 'lottie-web';
@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  busyRequestCount = 0;
  animation: any;
  constructor(private spinnerService: NgxSpinnerService) {

  }

  busy() {
    this.busyRequestCount++;
    if (this.busyRequestCount === 1) {  // Solo muestra cuando sea el primer request
      //this.animation.play();
      //document.getElementById('lottie-container')!.style.display = 'block';
    }
  }

  idle() {
    this.busyRequestCount--;
    if (this.busyRequestCount <= 0) {
      this.busyRequestCount = 0;
      //this.animation.stop();
      //document.getElementById('lottie-container')!.style.display = 'none';
    }

  }
}
