import { Injectable } from '@angular/core';
import { StorageBase } from './storage.base';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService extends StorageBase {
  constructor() {
    super(localStorage);
  }

  clearLocalStorage() {
    localStorage.removeItem('scope');
    localStorage.removeItem('RefreshToken');
    localStorage.removeItem('Token');
  }
}