export enum KEYS {
  USERDATA = 'user_data',
  USERDATAFINANZAS = 'user_data_finances',
  USERRECORD = 'user_record',
  CREATE_ADDRESS = 'bypass_create_address',
  STEP_ORDER_ADDRESS = 'step_order_address',
  CART_ORDER = 'card_order',
  USER_UBIGEO = "user_ubigeo",
  USERDATAIDSPRODUCT = 'ids_product',
  USERCODEREFERED = 'user_coderefered',
}
