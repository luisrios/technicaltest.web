export abstract class StorageBase {
  storage: any;
  base64Regexp: RegExp;

  constructor(storage: any) {
    this.storage = storage;
    this.base64Regexp = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
  }

  getItem(key: string): any {
    const item = this.storage.getItem(key);

    return item && this.base64Regexp.test(item) ? atob(item) : item;
  }

  getItemObject<T>(key: string): T {
    const item = this.getItem(key);

    return (item ? JSON.parse(item) : item) as T;
  }

  removeItem(key: string): void {
    this.storage.removeItem(key);
  }

  setItem<T = string>(key: string, data: T, encrypt = true): void {
    const value = !data ? '' : typeof data === 'object' ? JSON.stringify(data) : data;
    this.storage.setItem(key, encrypt ? btoa(value as string) : value);
  }

  clear(): void {
    this.storage.clear();
  }
  setItemCodeRefered<T = string>(key: string, data: T, encrypt = true): void {
    const value = !data ? '' : typeof data === 'object' ? JSON.stringify(data) : data;
    try {
      this.storage.setItem(key, encrypt ? btoa(unescape(encodeURIComponent(value as string))) : value);
    } catch (error) {
      this.clear();
    }

  }
}
