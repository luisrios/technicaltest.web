import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import * as signalR from '@aspnet/signalr';
import { IntermediarioService } from '../intermediary/intermediario.service';

@Injectable({
  providedIn: 'root',
})
export class SignalRService {
  hubConnection!: signalR.HubConnection;
  hubConnectionIntegracion!: signalR.HubConnection;

  constructor(private intermediarioService:IntermediarioService) {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('http://localhost:7776/usuario', {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets,
      }) // la URL del hub en tu servidor
      .build();
  }

  startConnection() {
    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch((err) => console.log('Error while starting connection: ' + err));
  }

  addEvent() {
    this.hubConnection.on('receiveMessage', (data: string) => {
      console.log('Nuevo usuario', data);
      this.intermediarioService.listaUsuario.push(data);
    });
  }
}
