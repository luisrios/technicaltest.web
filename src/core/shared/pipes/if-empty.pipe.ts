import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'ifEmpty'
})
export class IfEmptyPipe implements PipeTransform {

    transform(value: string | null | undefined, placeholder: string = '-'): string {
        return value ? value : placeholder;
    }

}
