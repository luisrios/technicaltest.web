import { NgModule } from '@angular/core';
import { IfEmptyPipe } from './if-empty.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [CommonModule],
    exports: [IfEmptyPipe],
    declarations: [IfEmptyPipe],
    providers: [],
})
export class PipesModule { }