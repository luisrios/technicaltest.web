import { NgModule } from '@angular/core';
import { PipesModule } from './pipes/pipes.module';
import { DirectivesModule } from './directives/directives.module';
import { ComponentsModule } from './components/components.module';

@NgModule({
    imports: [
        DirectivesModule,
        ComponentsModule,
        PipesModule
    ],
    exports: [
        DirectivesModule,
        ComponentsModule,
        PipesModule
    ],
    declarations: [],
    providers: [],
})
export class SharedModule { }