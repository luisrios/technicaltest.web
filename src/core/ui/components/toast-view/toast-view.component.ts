import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PrimeNGConfig } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { IToastConfig } from '../../../../app/settings/models/toast-config.interface';

@Component({
  selector: 'app-toast-view',
  templateUrl: './toast-view.component.html',
  styleUrls: ['./toast-view.component.css'],
  providers: [MessageService],
})
export class ToastViewComponent implements OnInit {
  @Input() objInputToast: IToastConfig = {};
  @Output() eventClick = new EventEmitter<any>();
  constructor(
    private config: PrimeNGConfig,
    public translateService: TranslateService,
    private messageService: MessageService,
  ) {}

  ngOnInit(): void {
    this.config.setTranslation({
      accept: 'Accept',
      reject: 'Cancel',
      //translations
    });
    this.setTranslate('es');
    
    setTimeout(() => {
      this.updateMessage();
    }, 2000);

    /*this.popupService.$getSourceObjectPopUpView.subscribe((data) => {
      this.objInputToast = data;
      if (this.objInputToast.key ?? 0 > 0){
        setTimeout(() => {
          this.updateMessage();
        }, 2000);
      }
    });*/
  }
  setTranslate(lang: string) {
    this.translate(lang);
    this.translateService.addLangs([lang]);
    this.translateService.setDefaultLang(lang);
    this.translate(lang);
  }

  translate(lang: string) {
    this.translateService.use(lang);
    this.translateService
      .stream('primeng')
      .subscribe((res) => this.config.setTranslation(res));
  }

  emitClickEvent(): void {
    this.eventClick.emit();
  }
  updateMessage() {
    this.messageService.add({
      key: this.objInputToast.key,
      severity: this.objInputToast.severity,
      summary: this.objInputToast.summary,
      detail: this.objInputToast.detail,
      life: this.objInputToast.life,
    });
  }
}
