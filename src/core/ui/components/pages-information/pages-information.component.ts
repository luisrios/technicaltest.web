import { Component, Input } from '@angular/core';
import { IPagesInformation } from './pages-information.interface';

@Component({
  selector: 'eco-pages-information',
  templateUrl: './pages-information.component.html',
  styleUrls: ['./pages-information.component.scss'],
})
export class PagesInformationComponent {
    @Input() pageInformation: IPagesInformation = {};
}