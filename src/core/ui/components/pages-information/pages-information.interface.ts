export interface IPagesInformation {
    title?: string;
    subtitle?: string;
    description?: string;
    description2?: string;
    description3?: string;
    buttonOkText?: string;
    image?: string
}