import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PagesInformationComponent } from './pages-information.component';

@NgModule({
  declarations: [PagesInformationComponent],
  imports: [CommonModule, RouterModule],
  exports: [PagesInformationComponent],
})
export class PagesInformationModule {}