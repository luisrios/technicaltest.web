import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoadingChildComponent } from './loading-child.component';

@NgModule({
  declarations: [LoadingChildComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [CommonModule, RouterModule, NgxSpinnerModule],
  exports: [LoadingChildComponent, NgxSpinnerModule],
})
export class LoadingChildModule {}