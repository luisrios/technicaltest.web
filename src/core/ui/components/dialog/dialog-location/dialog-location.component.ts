import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UbigeoIntemediaryService } from '@core/shared/common/services/intermediary/ubigeo.service';
import { LocalStorageService } from '@core/shared/common/services/storage';
import { KEYS } from '@core/shared/common/services/storage/keys-data.enum';
import { UbigeoService } from 'src/app/providers/services/ubigeo/ubigeo.service';
import { CODE_RESPONSE_VALUES } from 'src/app/settings/constants/code-response-values.constants';
import {
  IUbigeo,
  IUbigeoMemory,
} from 'src/app/settings/models/ubigeo.interface';

@Component({
  selector: 'eco-dialog-location',
  templateUrl: './dialog-location.component.html',
  styleUrls: ['./dialog-location.component.scss'],
})
export class DialogLocationComponent implements OnInit {
  @Input() visibleDialogLocation: boolean = false;
  @Output() hideDialog: EventEmitter<boolean> = new EventEmitter();
  @Output() actionSaveUbigeo: EventEmitter<boolean> = new EventEmitter();

  dataDepartments: IUbigeo[] = [];
  dataProvinces: IUbigeo[] = [];
  dataDistricts: IUbigeo[] = [];
  ubigeoSession: IUbigeoMemory = {};
  formUbigeo!: FormGroup;

  constructor(
    private ubigeoService: UbigeoService,
    private fb: FormBuilder,
    private storageService: LocalStorageService,
    private ubigeoIntermediaryService: UbigeoIntemediaryService
  ) {}

  ngOnInit() {
    this.getDepartments();
    this.createForm();

    this.ubigeoIntermediaryService.$getSourceObjectLoadUbigeo.subscribe((value) => {
      if(value){
        this.createForm();
      }
    });
  }

  hideDialogLocation() {
    this.hideDialog.emit();

    this.ubigeoSession = this.getUbigeoSession();
    if (this.ubigeoSession != null) {
      this.formUbigeo.controls['departmentId'].patchValue(
        this.ubigeoSession.deparmtent?.id
      );
      this.formUbigeo.controls['provinceId'].patchValue(
        this.ubigeoSession.province?.id
      );
      this.formUbigeo.controls['districtId'].patchValue(
        this.ubigeoSession.district?.id
      );
    }
  }
  getUbigeoSession(): IUbigeoMemory {
    let userUbigeoStorage = this.storageService.getItem(KEYS.USER_UBIGEO);
    return userUbigeoStorage !== ''
      ? JSON.parse(this.storageService.getItem(KEYS.USER_UBIGEO))
      : null;
  }
  createForm() {
    this.ubigeoSession = this.getUbigeoSession();

    this.formUbigeo = this.fb.group({
      departmentId: [null, Validators.required],
      provinceId: [null, Validators.required],
      districtId: [null, Validators.required],
    });

    if (this.ubigeoSession != null && this.ubigeoSession) {
      this.formUbigeo.controls['departmentId'].patchValue(
        this.ubigeoSession.deparmtent?.id
      );
      this.getProvinces(this.ubigeoSession.deparmtent?.id ?? 0);
      this.formUbigeo.controls['provinceId'].patchValue(
        this.ubigeoSession.province?.id
      );
      this.getDistricts(this.ubigeoSession.province?.id ?? 0);
      this.formUbigeo.controls['districtId'].patchValue(
        this.ubigeoSession.district?.id
      );
    } else {
      this.ubigeoSession = {};
      this.formUbigeo.controls['departmentId'].patchValue(null);
      this.formUbigeo.controls['provinceId'].patchValue(null);
      this.formUbigeo.controls['districtId'].patchValue(null);
    }

    /*  this.formUbigeo?.get('deparmentId')?.valueChanges.subscribe((val) => {
      console.log('valueChanges deparmentId', val);
      this.getProvinces(val);
      this.formUbigeo.controls['provinceID'].patchValue(null);
      this.formUbigeo.controls['districtID'].patchValue(null);
      this.dataProvinces = [];
    });

    this.formUbigeo?.get('provinceId')?.valueChanges.subscribe((val) => {
      this.getDistricts(val);
      this.formUbigeo.controls['districtID'].patchValue(null);
      this.dataDistricts = [];
    });*/
  }
  getDepartments() {
    this.ubigeoService
      .getDepartments()
      .subscribe((response) => {
        if (response.code == CODE_RESPONSE_VALUES.Ok) {
          this.dataDepartments = response.payload || [];
        }
      })
      .add(() => {});
  }
  getProvinces(id: number) {
    this.ubigeoService.getProvincesByDepartmentID(id).subscribe((response) => {
      if (response.code == CODE_RESPONSE_VALUES.Ok) {
        this.dataProvinces = response.payload || [];
      }
    });
  }
  getDistricts(id: number) {
    this.ubigeoService.getDistrictsByProvinceID(id).subscribe((response) => {
      if (response.code == CODE_RESPONSE_VALUES.Ok) {
        this.dataDistricts = response.payload || [];
      }
    });
  }
  selectDepartment(event: any) {
    this.getProvinces(event.value);
    this.dataDistricts = [];
    let department = this.dataDepartments.find((x) => x.id == event.value);
    this.ubigeoSession.deparmtent = department;
  }
  selectProvinces(event: any) {
    this.getDistricts(event.value);
    let province = this.dataProvinces.find((x) => x.id == event.value);
    this.ubigeoSession.province = province;
  }
  selectDistrict(event: any) {
    let district = this.dataDistricts.find((x) => x.id == event.value);
    this.ubigeoSession.district = district;
  }
  saveUbigeo() {
    console.log('ubigeoSession', this.ubigeoSession);
    this.storageService.setItem(KEYS.USER_UBIGEO, this.ubigeoSession);
    this.hideDialogLocation();
    this.actionSaveUbigeo.emit();
    this.visibleDialogLocation = false;

    this.ubigeoIntermediaryService.sendObjectLoadServices(true);
  }
}
