import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogLocationComponent } from './dialog-location.component';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [DialogLocationComponent],
  exports: [DialogLocationComponent]
})
export class DialogLocationModule { }
