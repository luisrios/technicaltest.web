import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BannerService } from 'src/app/providers/services/banner/banner.service';

@Component({
  selector: 'eco-dialog-banner',
  templateUrl: './dialog-banner.component.html',
  styleUrls: ['./dialog-banner.component.scss']
})
export class DialogBannerComponent implements OnInit {

  @Input() visibleDialogBanner: boolean = false;
  @Output() hideBanner: EventEmitter<boolean> = new EventEmitter();

  constructor(public _serviceBanner: BannerService) { }

  ngOnInit() {
  }

  hideDialogBanner() {
    this.hideBanner.emit()
  }

}
