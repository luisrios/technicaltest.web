import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogBannerComponent } from './dialog-banner.component';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  imports: [
    CommonModule,
    DialogModule,
  ],
  declarations: [DialogBannerComponent],
  exports: [DialogBannerComponent]
})
export class DialogBannerModule { }
