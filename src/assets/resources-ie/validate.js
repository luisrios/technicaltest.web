window.onload = function() {
    var browser = navigator.userAgent.search(/(?:Edge|MSIE|Trident\/.*; rv:)/);
  if(browser > -1) {
      document.getElementById("divSection").style.display = 'block';
      document.getElementById("divHeader").style.display = 'flex';
  } else {
    document.getElementById("divSection").style.display = 'none';
    document.getElementById("divHeader").style.display = 'none';
  }
};